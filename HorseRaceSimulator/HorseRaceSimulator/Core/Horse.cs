﻿using System;

namespace HorseRaceSimulator.Core
{    class Horse
    {
        public String HorseName { get; set; }
        public double Odd { get; set; }
        public double Margin
        {
            get
            {
                return (100 / (Odd + 1));
            }
        }
        public double ChanceOfWinning { get; set; }

    }
}
