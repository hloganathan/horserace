﻿using HorseRaceSimulator.Core;
using HorseRaceSimulator.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace HorseRaceSimulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            List<Horse> horses = new List<Horse>();
            horses.Add(new Horse { HorseName = "Teddy", Odd = 0.5 });
            horses.Add(new Horse { HorseName = "Jerry", Odd = 2 });
            horses.Add(new Horse { HorseName = "Tom", Odd = 3 });
            horses.Add(new Horse { HorseName = "Jack", Odd = 8 });

            HorseRace horseRace = new HorseRace(horses, 110, 140);      

            if (horseRace.IsValidRace())
            {               
                horseRace.Winner();
            }

        }

    }
}
